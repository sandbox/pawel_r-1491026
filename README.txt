---SUMMARY---
Wysiwyg CKEditor Default Language is additional module for WYSIWYG (http://drupal.org/project/wysiwyg) used with CKEditor (http://ckeditor.com/) editor library.

Purpose is simple: language used by CKEditor UI (configured as WYSIWYG's profile) is the same as language currently used by user.

Works only for CKEditor (no compatibility with any other editor used by WYSIWYG module).

---INSTALLATION---
Enable module on module list

---CONFIGURATION---
No configuration needed.

---LIMITATIONS---
This moduel works for languages that are available for CKEditor library.

---OTHER INFO---

Module based on: http://drupal.org/node/532794#comment-3034814
